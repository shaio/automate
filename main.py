from telegram.ext import Updater, CommandHandler
import hug
import json
import importlib

from utils.TelegramWrapper import TelegramWrapper

import config

with open('opciones.json') as config_file:
    opciones_json = json.load(config_file)

router = hug.route.API(__name__)
updater = Updater(token=config.telegram_token, request_kwargs={'read_timeout': 6, 'connect_timeout': 7})
dispatcher = updater.dispatcher

opciones = opciones_json['opciones']
roothpath = '/{path}'

for opcion in opciones:
    path = opcion.get('path', opcion['nombre'])
    funcion_completa = opcion.get('funcion')
    funcion_separada = funcion_completa.split('/')
    modulo = importlib.import_module(funcion_separada[0])
    funcion = getattr(modulo, funcion_separada[1])

    router.get(roothpath.format(path=path))(funcion)

    telegram_wrapper = TelegramWrapper(funcion)
    handler = CommandHandler(path, telegram_wrapper.get_function_response, pass_args=True)
    dispatcher.add_handler(handler)

updater.start_polling()
