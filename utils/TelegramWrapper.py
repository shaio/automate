class TelegramWrapper:
    def __init__(self, funcion):
        self._funcion = funcion

    def get_function_response(self, bot, update, args=None, **kwargs):
        response = self._funcion()

        bot.send_message(chat_id=update.message.chat_id, text=response, **kwargs)

